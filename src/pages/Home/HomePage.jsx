import Title from "../../components/Title";

function HomePage() {
  return (
    <>
      <Title content="Home Page" />
      <p>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ea fuga non
        officia vel pariatur error illo maxime velit accusamus, ipsum deserunt
        veniam temporibus qui mollitia exercitationem soluta hic alias ad.
      </p>
    </>
  );
}

export default HomePage;
