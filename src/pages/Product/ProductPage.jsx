import Title from "../../components/Title";

function ProductPage() {
  return (
    <>
      <Title content="Product list" />
      <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sed ipsa enim
        praesentium, voluptate earum adipisci fuga veniam voluptatibus
        consectetur temporibus officia, necessitatibus officiis doloribus
        maiores optio blanditiis nisi sint exercitationem!
      </p>
    </>
  );
}

export default ProductPage;
