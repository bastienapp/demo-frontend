/* eslint-disable react/prop-types */
function Title(props) {
  return <h3>{props.content}</h3>;
}

export default Title;
