import { Link, Outlet } from "react-router-dom";

function Root() {
  return (
    <>
      <header>
        <h1>Demo website</h1>
        <nav>
          <ul>
            <li>
              <Link to={`/`}>Home</Link>
            </li>
            <li>
              <Link to={`/products`}>Products</Link>
            </li>
          </ul>
        </nav>
      </header>
      <main>
        <Outlet />
      </main>
      <footer>©20xx</footer>
    </>
  );
}

export default Root;
